Feed Extension for Mecha
========================

Release Notes
-------------

### 2.4.2

 - Removed sitemap feature. The sitemap feature should be added to a separate extension named `sitemap`.
 - Added `image` property to feeds.
